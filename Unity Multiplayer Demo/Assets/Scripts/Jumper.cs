﻿using UnityEngine;

public class Jumper : MonoBehaviour
{

    public float jumpPower;
    public AudioSource audioSource;

    void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision other)
    {
        Debug.Log("Enter");
        other.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpPower);
        audioSource.Play();
    }

}
