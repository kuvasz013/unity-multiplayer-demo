﻿using UnityEngine;
using UnityEngine.Networking;

public class GameManager : NetworkManager
{

    public override void OnClientConnect(NetworkConnection conn)
    {
        ClientScene.AddPlayer(conn, 0);
    }
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        GameObject player = (GameObject)Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);

        // Set the color of the spawned player
        Color playerColor = new Color(
            Random.Range(0f, 1f),
            Random.Range(0f, 1f),
            Random.Range(0f, 1f)
        );
        player.GetComponent<Renderer>().sharedMaterial.SetColor("_Color", playerColor);

        // Create CameraRig and set instances
        GameObject cameraRig = Instantiate(spawnPrefabs[0], playerPrefab.transform.position, playerPrefab.transform.rotation);

        //FIXME: Player gets instantiated, but cannot move, removed player GO not cant be destroyed
        bool success = NetworkServer.AddPlayerForConnection(conn, playerPrefab, playerControllerId);

        cameraRig.GetComponent<CameraController>().player = player.transform;
        player.GetComponent<PlayerController>().cameraRig = cameraRig.GetComponent<Camera>();
    }

}
