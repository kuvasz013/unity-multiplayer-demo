﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{

    private Rigidbody rb;
    private AudioSource audioSource;
    public Camera cameraRig;
    private float jumpPower;
    private NetworkIdentity identity;
    public float sprintMultiplier;
    public float force;
    public float jump;
    public float maxJumpPower;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        identity = gameObject.GetComponentInParent<NetworkIdentity>();
    }

    private void FixedUpdate()
    {
        if (!identity.isLocalPlayer)
        {
            return;
        }

        float sprint = Input.GetKey(KeyCode.LeftShift) ? sprintMultiplier : 1f;

        float inputForward = Input.GetAxis("Vertical");
        rb.AddForce(cameraRig.transform.TransformDirection(Vector3.forward * inputForward * force * sprint));

        float inputSideways = Input.GetAxis("Horizontal");
        rb.AddForce(cameraRig.transform.TransformDirection(Vector3.right * inputSideways * force * sprint));

        if (Input.GetKey(KeyCode.Space))
        {
            jumpPower += jump;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        audioSource.Play();
        Debug.DrawLine(other.GetContact(0).point, other.GetContact(0).point + other.impulse.normalized, Color.green, 2f, false);

        if (!identity.isLocalPlayer)
        {
            return;
        }

        if (Input.GetKey(KeyCode.Space) && other.gameObject.tag == "Bounce")
        {
            Vector3 force = Vector3.up * (jumpPower >= maxJumpPower ? maxJumpPower : jumpPower + 200);
            Debug.Log(force);
            rb.AddForce(Vector3.up * (jumpPower >= maxJumpPower ? maxJumpPower : jumpPower + 200));
        }
        jumpPower = 0;
    }
}
