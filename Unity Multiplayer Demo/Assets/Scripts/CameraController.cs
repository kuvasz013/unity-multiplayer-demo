﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Transform camTransform;
    public Transform player;
    public float mouseSensitivityX = 3f;
    public float mouseSensitivityY = 1.5f;
    float verticalLookRotation;
    public float minCamRotationY = -25f;
    public float maxCamRotationY = -5f;

    void Start()
    {
        camTransform = GetComponentInChildren<Camera>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.position;
        transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * mouseSensitivityX);
        verticalLookRotation += Input.GetAxis("Mouse Y") * mouseSensitivityY;
        verticalLookRotation = Mathf.Clamp(verticalLookRotation, minCamRotationY, maxCamRotationY);
        camTransform.localEulerAngles = Vector3.left * verticalLookRotation;
    }
}
